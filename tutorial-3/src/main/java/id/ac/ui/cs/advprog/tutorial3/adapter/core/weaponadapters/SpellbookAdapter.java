package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private Boolean isCharged;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.isCharged = false;
    }

    @Override
    public String normalAttack() {
        isCharged = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if(isCharged==false){
            isCharged=true;
            return spellbook.largeSpell();
        }
        else{
            isCharged=false;
            return "Magic power not enough for large spell";
        }
    }

    @Override
    public String getName() {

        return spellbook.getName();
    }

    @Override
    public String getHolderName() {

        return spellbook.getHolderName();
    }

}
