package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private BowRepository bowRepository;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        if(weaponRepository.findAll().size()<=4) {
            for (Bow bow : bowRepository.findAll()) {
                BowAdapter bowWeapon = new BowAdapter(bow);
                weaponRepository.save(bowWeapon);
            }
            for (Spellbook spellbook : spellbookRepository.findAll()) {
                SpellbookAdapter spellbookWeapon = new SpellbookAdapter(spellbook);
                weaponRepository.save(spellbookWeapon);
            }
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        String log = weapon.getHolderName() + " attacked with " + weapon.getName();
        if (attackType==1){
            log+=" (normal attack): " + weapon.normalAttack();
            logRepository.addLog(log);
            weaponRepository.save(weapon);
        }
        else if(attackType==2){
            log+=" (charged attack): " + weapon.chargedAttack();
            logRepository.addLog(log);
            weaponRepository.save(weapon);
        }
    }
    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
