package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    //ToDo: Complete me
    public AttackWithMagic(){}

    public String attack (){
        return "Attack with Magic";
    }
    public String getType (){
        return "Attack with Magic";
    }
}
