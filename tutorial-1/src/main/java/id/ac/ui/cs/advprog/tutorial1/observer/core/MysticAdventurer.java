package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
        //ToDo: Complete Me
    }

    //ToDo: Complete Me
    public void update() {
        Quest quest = guild.getQuest();
        if (guild.getQuestType().equals("D")){
            this.getQuests().add(quest);
        }
        else if (guild.getQuestType().equals("E")){
            this.getQuests().add(quest);
        }
    }
}
