package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
    }

    @Override
    public void update() {
        Quest quest = guild.getQuest();
        if (guild.getQuestType().equals("D")) {
            this.getQuests().add(quest);
        }
        else if (guild.getQuestType().equals("R")){
            this.getQuests().add(quest);
        }
    }
}
