package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    //ToDo: Complete me
    public DefendWithBarrier(){}

    public String defend (){
        return "Defend with Barrier";
    }

    public String getType (){
        return "Defend with Barrier";
    }
}
