package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        this.guild = guild;
        //ToDo: Complete Me
    }

    //ToDo: Complete Me
    @Override
    public void update() {
        Quest quest = guild.getQuest();
        this.getQuests().add(quest);
    }
}
